console.log("Hellow Wurld?");

// [ SECTION ] Arithmetic Operators
	let numA = 105;
	let numB = 6;

	// What we want is to get the sum of numA and numB
	let sum = numA + numB;
	console.log('Result of addition operator: ' + sum);

	// What we want is to get the difference of numA and numB
	let difference = numA - numB;
	console.log('Result of subtraction operator: ' + difference); 

	// What we want is to get the product of numA and numB
	let product = numA * numB;
	console.log('Result of multiplication operator: ' + product);

	// What we want is to get the quotient of numA and numB
	let quotient = numA / numB;
	console.log('Result of division operator: ' + quotient);

	// remainder/modulo
	let remainder = numA % numB;
	console.log('Result of modulo operator: ' + remainder);

		// numA = 25;
		// numB = 25
		// sum = numA + numB;
		// console.log(sum);

// [ SECTION ] Assignment Operators

	// Basic Assignment Operator (=)
		// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable 

			let assignedNumber = 8;
			console.log(assignedNumber);

	// Addition Assignment Operator (+=)
        // The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable. 

			assignedNumber = assignedNumber + 2;
			console.log("Result of addition assignment operator: " + assignedNumber);

		// Asan dun si Addition Assignment Operator?

			// Shorthand for assignmentNumber = assignmentNumber + 2 
			assignedNumber += 2;
			console.log("Result of addition assignment operator: " + assignedNumber);

			// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
		        assignedNumber -= 2; 
		        console.log("Result of subtraction assignment operator: " + assignedNumber);

		        assignedNumber *= 2; 
		        console.log("Result of multiplication assignment operator: " + assignedNumber);

		        assignedNumber /= 2; 
		        console.log("Result of division assignment operator: " + assignedNumber);

// [ SECTION ] Multiple Operators and Parentheses

		let mdas = 1 + 2 -3 * 4 / 5; // 1 + 2 - ((3 * 4) / 5)
		console.log('Result of mdas operation: ' + mdas);
	/*
 		- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule

         	- The operations were done in the following order:
         		// 1 + 2 - ((3 * 4) / 5)

	                1. 3 * 4 = 12
	                2. 12 / 5 = 2.4
	                3. 1 + 2 = 3
	                4. 3 - 2.4 = 0.6
	*/
		let pemdas = 1 + (2 - 3) * (4 / 5);
		 /*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
        */
        console.log("Result of pemdas operation: " + pemdas);

        pemdas = (1 + (2 - 3)) * (4 / 5);
                /*
            - By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. 1 + -1 = 0
                4. 0 * 0.8 = 0
        */
        // console.log("Result of pemdas operation: " + pemdas);

// [ SECTION ] Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to 

        // Pre-Increment 
	        let blankets = 10; // May Continuity
	        let newBlankets = ++blankets;
	        console.log("blankets: " + blankets);
	        console.log("newBlankets: " + newBlankets);



        // Post-Increment
	        let pillows = 10; // Walang Continuity
	        let newPillows = pillows++;
	        console.log("pillows: " + pillows);
	        console.log("newPillows: " + newPillows);


	        // Observation: Both Pre-Increment & Post-Increment adds 1 to its initial value but the main difference is their post-value. 


	    // Pre-Decrement
	        let soda = 10; // May Continuity
	        let newSoda = --soda;
	        console.log("soda: " + soda);
	        console.log("newSoda: " + newSoda);

	    // Post-Decrement
	        let pasta = 10;  // Walang Continuity
	        let newPasta = pasta--;
	        console.log('pasta: ' + pasta);
	        console.log('newPasta: ' + newPasta);

// [ SECTION ] Type Coersion

	/*
		- Type coercion is the automatic or implicit conversion of values from one data type to another
        - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
        - Values are automatically converted from one data type to another in order to resolve operations

	*/

				let num1 = '10'; // Recognized as string
				let num2 = 12; // Recognized as number

				console.log(num1);
				console.log(num2);

		/*
			- Adding/concatenating a string and a number will result is a string
            - This can be proven in the console by looking at the color of the text displayed
            - Black text means that the output returned is a string data type
		*/

		let coersion = num1 + num2;
		console.log(coersion);
		console.log(typeof coersion);

				let num3 = 14;
				let num4 = 12;

				let nonCoersion = num3 + num4;
				console.log(nonCoersion);
				console.log(typeof nonCoersion);

		 /* 
            - The result is a number
            - This can be proven in the console by looking at the color of the text displayed
            - Blue text means that the output returned is a number data type
        */

				let num5 = true + 1;
				console.log(num5); // Result would be, 2

		/* 
            - The result is a number
            - The boolean "true" is also associated with the value of 1
        */

				let num6 = false + 1;
				console.log(num6); // Result would be, 1

		 /* 
            - The result is a number
            - The boolean "false" is also associated with the value of 0
        */

// [ SECTION ] Comparsion Operators

	let juan = 'juan';

		// Equality Operator (==) To check if they are the same

			// - Checks whether the operands are equal/have the same content
			// - Attempts to CONVERT AND COMPARE operands of different data types
			// - Returns a boolean value

			console.log(1 == 1); // Result would be, true
			console.log(1 == 2); // Result would be, false

			console.log(1 == '1'); // Result would be, true
			console.log(0 == false); // Result would be, true

				// Compares two strings that are the same
					console.log('juan' == 'juan'); // Result would be, true

				// Compares a string with the variable 'juan' declared above
					console.log('juan' == juan); // Result would be, true

		
		// Inequality Operator (!=) To check if they are not the same

			// - Checks whether the operands are not equal/have different content
            // - Attempts to CONVERT AND COMPARE operands of different data types

            console.log(1 != 1); // Result would be, false
            console.log(1 != 2); // Result would be, true

            console.log(1 != '1'); // Result would be, false
			console.log(0 != false); // Result would be, false

			console.log('juan' != 'juan'); // Result would be, false
        	console.log('juan' != juan); // Result would be, false

        // Strict Equality Operator (===)

        	//- Checks whether the operands are equal/have the same content
            //- Also COMPARES the data types of 2 values
        	//- JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
           	//- In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            //- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            //- Strict equality operators are better to use in most cases to ensure that data types provided are correct

            console.log(1 === 1); // Results would be, true
            console.log(1 === 2); // Results would be, false

            console.log(1 === '1'); // Results would be, false
            console.log(0 === false); // Results would be, false

            console.log('juan' === 'juan'); // Results would be, true
            console.log('juan' === juan); // Results would be, true

        // Strict Inequality Operator (!==) 

            //- Checks whether the operands are not equal/have the same content
            //- Also COMPARES the data types of 2 values

            console.log(1 !== 1); // Results would be, false
            console.log(1 !== 2); // Results would be, true

            console.log(1 !== '1'); // Results would be, true
            console.log(0 !== false); // Results would be, true

            console.log('juan' !== 'juan'); // Results would be, false
            console.log('juan' !== juan); // Results would be, false

// [ SECTION ] Relational Operators
	// Some comparison operators check whether one value is greater or less than to the other value.

    let a = 50;
    let b = 65;
    let c = 1

    	// GT or Greater Than Operator ( > )
    		let isGreaterThan = a < b; 
    		console.log(isGreaterThan); // Result would be, false

    	// LT or Less Than Operator ( < )
    		let isLessThan = a < b;
    		console.log(isLessThan); // Result would be, true

    	// GTE or Greater Than or Equal Operator ( >= )
    		let isGtOrEqual = a >= b;
    		console.log(isGtOrEqual); // Result would be, false

    	// LTE or Less Than or Equal Operator ( <= )
    		let isLtorEqual = a <= b;
    		console.log(isLtorEqual); // Result would be, true

    	let numString = '30';
    	console.log(a > numString); // Result would be, true
    	console.log(b <= numString); // Result would be, false

    	let string = 'twenty';
    	console.log( b >= string); // Result would be, false
    		//- Since the string is not numeric, The string was converted to a number and it 
        	//- Resulted to NaN. 65 is not greater than NaN.

// [ SECTION ] Logical Operators

    	let isLegalAge = true;
    	let isRegistered = false;

    		// Logical And Operator (&& - Double Ampersand) // If both of the is true
        		// Returns true if all operands are true 
		        let allRequirementsMet = isLegalAge && isRegistered;
		        console.log("Result of logical AND Operator: " + allRequirementsMet);

		    // Logical Or Operator (|| - Double Pipe) // If one is true
		        // Returns true if one of the operands are true 
		        let someRequirementsMet = isLegalAge || isRegistered;
		        console.log("Result of logical OR Operator: " + someRequirementsMet);

		    // Logical Not Operator (! - Exclamation Point)
		        // Returns the opposite value 
		        let someRequirementsNotMet = !isRegistered;
		        console.log("Result of logical NOT Operator: " + someRequirementsNotMet);
